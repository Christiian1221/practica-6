package mx.unitec.moviles.practica6.widget

import androidx.fragment.app.DialogFragment
import mx.unitec.moviles.practica6.model.Contact

interface NoticeDialogListener {
    fun onDialogPositiveClick(dialog: DialogFragment, contact: Contact)
    fun onDialogNegativeClick(dialog: DialogFragment)
}